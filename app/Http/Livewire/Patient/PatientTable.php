<?php

namespace App\Http\Livewire\Patient;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Redirect;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Patient;

class PatientTable extends DataTableComponent
{
    protected $listeners=[
        'refresh_patient_table' => '$refresh'
    ];

    public function columns(): array
    {
        return [
            Column::make('First name','first_name')->searchable(),
            Column::make('Last name','last_name')->searchable(),
            Column::make('Email','email')->searchable(),
            Column::make('Phone Number','phone')->searchable(),
            Column::make('Address','address')->searchable(),
            Column::make('Actions'),
        ];
    }

    public function query(): Builder
    {
        return Patient::query()->orderByDesc('id');
    }

    public function rowView(): string
    {
        return 'livewire-tables.rows.patient_table';
    }

    public function show($id){
        return Redirect::route('patients.show',$id);
    }
}
