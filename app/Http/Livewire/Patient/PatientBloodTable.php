<?php

namespace App\Http\Livewire\Patient;

use App\Models\BloodPressure;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class PatientBloodTable extends DataTableComponent
{
    public $patient_id;
    protected $listeners = ['refresh_patient_blood_table' => '$refresh'];
    public function __construct($patient_id)
    {
        parent::__construct($patient_id);
    }

    public function columns(): array
    {
        return [
            Column::make('Blood pressure','blood_pressure'),
            Column::make('Recorded at','created_at'),
            Column::make('Recorded by'),
        ];
    }

    public function query(): Builder
    {
        return BloodPressure::query()->where('patient_id','=',$this->patient_id)->with('recorded_by');
    }

    public function rowView(): string
    {
        return 'livewire-tables.rows.patient_blood_table';
    }
}
