<div>

        <x-jet-button class="btn btn-primary" wire:click="export()" wire:loading.attr="disabled">
            <span wire:loading.remove>
              Export CSV
            </span>

            <span wire:loading>
                File Downloading ...
            </span>
        </x-jet-button>
    <!-- <x-jet-button wire:click="export()"  >

    </x-jet-button> -->
    <x-jet-button wire:click="addpatient" class="bg-blue-500 hover:bg-blue-700">
        Add New Patient
    </x-jet-button>

    <!-- Modal -->


        <x-jet-dialog-modal wire:model="addpatient">
            <x-slot name="title">
                Add patient
            </x-slot>

            <x-slot name="content">
                <div>
                    <x-jet-label for="first_name" :value="__('messages.first_name')" />
                    <x-jet-input class="block mt-1 w-full" wire:model="first_name"
                                 required autofocus :errors="$errors"/>
                    @error('first_name') <span class="h-2 mr-2 bg-red-600 text-white rounded-full">{{ $message }}</span> @enderror
                </div>
                <div>
                    <x-jet-label for="last_name" :value="__('messages.last_name')" />
                    <x-jet-input class="block mt-1 w-full" wire:model="last_name"
                                 required autofocus />
                    @error('last_name') <span class="h-2 mr-2 bg-red-600 text-white rounded-full">{{ $message }}</span> @enderror
                </div>
                <div>
                    <x-jet-label for="email" :value="__('messages.email')" />
                    <x-jet-input class="block mt-1 w-full" wire:model="email"
                                 required autofocus />
                    @error('email') <span class="h-2 mr-2 bg-red-600 text-white rounded-full">{{ $message }}</span> @enderror
                </div>
                <div>
                    <x-jet-label for="phone" :value="__('messages.phone')" />
                    <x-jet-input class="block mt-1 w-full" wire:model="phone"
                                 required autofocus />
                    @error('phone') <span class="h-2 mr-2 bg-red-600 text-white rounded-full">{{ $message }}</span> @enderror
                </div>
                <div>
                    <x-jet-label for="address" :value="__('messages.address')" />
                    <x-jet-input class="block mt-1 w-full" wire:model="address"
                                 required autofocus />
                    @error('address') <span class="h-2 mr-2 bg-red-600 text-white rounded-full">{{ $message }}</span> @enderror
                </div>
                <div>
                    <x-jet-label for="description" :value="__('messages.description')" />
                    <x-jet-input class="block mt-1 w-full" wire:model="description"
                                 required autofocus />
                    @error('description') <span class="h-2 mr-2 bg-red-600 text-white rounded-full">{{ $message }}</span> @enderror
                </div>
                <x-slot name="footer">
                    <x-jet-danger-button class="ml-2" wire:click="submit()" wire:loading.attr="disabled">
                        {{ __('Save') }}
                    </x-jet-danger-button>
                </x-slot>
            </x-slot>
        </x-jet-dialog-modal>
    <script>
        function f() {
            Toast.fire({
                icon: 'success',
                title: 'Download Started'
            })
        }
    </script>
</div>
