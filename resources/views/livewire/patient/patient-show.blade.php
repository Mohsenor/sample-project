<div>
    <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
        <div class="p-6 bg-white border-b border-gray-200">
            <h2>First name  : {{ $patient->first_name }}</h2>
            <h2>Last name  : {{ $patient->last_name }}</h2>
            <h2>Email  : {{ $patient->email }}</h2>
            <h2>Address  : {{ $patient->address }}</h2>
            <h2>Phone number  : {{ $patient->phone }}</h2>
            <h2>Description  : {{ $patient->description }}</h2>
        </div>
        <header class="flex h-10 bg-gray-200">Create new blood record</header>
        <div class="p-6 bg-white border-b border-gray-200">
            <form wire:submit.prevent="save_blood_record()">
                <div>
                    <x-jet-label for="blood_pressure" :value="__('messages.blood_pressure')" />
                    <x-jet-input class="block mt-1 w-full" wire:model="blood_pressure"
                             required autofocus />
                    @error('blood_pressure') <span class="h-2 mr-2 bg-red-600 text-white rounded-full">{{ $message }}</span> @enderror

                </div>
                <x-jet-secondary-button type="submit">
                    Save
                </x-jet-secondary-button>
            </form>
        </div>
    </div>
</div>

