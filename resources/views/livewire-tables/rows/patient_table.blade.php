<x-livewire-tables::table.cell>
    {{ $row->first_name }}
</x-livewire-tables::table.cell>
<x-livewire-tables::table.cell>
    {{ $row->last_name }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->email }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->phone }}
</x-livewire-tables::table.cell>
<x-livewire-tables::table.cell>
    {{ $row->address }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <x-jet-button class="bg-blue-500 text-white-700" wire:click="show({{$row->id}})">
        Show
    </x-jet-button>

</x-livewire-tables::table.cell>
