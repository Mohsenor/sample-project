<?php

namespace Tests\Feature;

use App\Http\Livewire\Patient\PatientShow;
use App\Models\BloodPressure;
use App\Models\Patient;
use App\Models\User;

use Livewire\Livewire;
use Tests\TestCase;

class CreateBloodSampleTest extends TestCase
{
    public function test_create_blood_pressure_record()
    {
        $this->actingAs($user = User::factory()->create());
        $patient = Patient::factory()->create();
        Livewire::test(PatientShow::class,['patient'=>$patient])
            ->set('blood_pressure', '52')
            ->call('save_blood_record');
        $this->assertTrue(BloodPressure::query()->where('blood_pressure','=','52')->latest()->exists());
        BloodPressure::query()->where('blood_pressure','=','52')->latest()->forceDelete();
        $user->forceDelete();
        $patient->forceDelete();
    }

    function test_find_show_patient_details()
    {
        $this->actingAs($user = User::factory()->create());
        $this->get('/patients/show/'.$user->id)->assertSeeLivewire('patient.patient-show');
        $user->forceDelete();
    }

    function test_blood_pressure_is_a_number()
    {
        $this->actingAs($user=User::factory()->create());
        $patient = Patient::factory()->create();
        Livewire::test(PatientShow::class,['patient'=>$patient])
            ->set('blood_pressure', 'rzd')
            ->call('save_blood_record')
            ->assertHasErrors();
        $patient->delete();
        $user->delete();
    }
}
